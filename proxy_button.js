/*
this wasn't working
document.getElementById("proxyButton").onClick=function() {
  alert(1);
  document.getElementById("proxyInfo").style.display = "inline";
  document.getElementById("proxyInfo").style.visibility = "shown";
  var configDirect = {
    mode: "direct"
  };
  chrome.proxy.settings.set({value: configDirect, scope: 'regular'});
};*/

/*so i copied this button eventlistener from stackoverflow
the rest of the code is "mine" (googled all of it)*/
var proxyIsEnabled = false;

document.addEventListener('DOMContentLoaded', function() {
  document.getElementById("proxyButton").addEventListener('click', function() {
    //see if the proxy is already enabled
    chrome.proxy.settings.get({"incognito": false}, function(config) {
      if(config.value.mode == "pac_script") proxyIsEnabled = true;
    });
    
    //toggle proxy on/off
    if(proxyIsEnabled === true) {
      var configDirect = {
        mode: "direct"
      };
      chrome.proxy.settings.set({value: configDirect, scope: "regular"});
      document.getElementById("proxyInfo").innerHTML = "GG enabled";
      document.getElementById("proxyInfo").style.display = "inline";
      proxyIsEnabled = false;
    }
    
    else {
      proxyIsEnabled = true;
      var configProxy = {
        mode: "pac_script",
        pacScript: {
          data: "function FindProxyForURL(url, host) {if(host.includes('goguardian.com')) {return 'PROXY blackhole:12345'} return 'DIRECT'};",
          mandatory: true
        }
      };
      
      chrome.proxy.settings.set({value: configProxy, scope: "regular"});
      document.getElementById("proxyInfo").innerHTML = "GG disabled";
      document.getElementById("proxyInfo").style.display = "inline";
    }
  });
});