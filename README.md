# school-browser

Browser that uses special proxy methods to bypass almost all known filters (GoGuardian, Relay, etc). 


## Getting started on windows

If you're using **WINDOWS**,follow these steps:

- ```mkdir browser```

- `git clone https://gitlab.com/GeekyFeller/school-browser`

- `cd browser`
- `cd school-browser`

(Side note,blame GitLab for not allowing a copy button cause it broke the README :/)

After that,just double click "browser.html"

## Getting started on a Chromebook
Chromebooks are still a little buggy,but I recommend you try to host this project on repl (repl.it) if you're on a Chromebook.

## Spread the word!
If you can, you can upload all these files to a Google Developer account and publish it as an extension to help other people out!

## Contributing
Just make a merge request and i'll read and approve it eventually.
## Authors and acknowledgment
Thank you to Chromium for the cream of the crop (core) of the project. 
## License
None. Use this however you want. Skid and claim it as your own,I could care less. Giving credit would be very nice :>) 
## Project status
Currently somewhat frozen, I'm preparing a new version that uses the actual Chrome source (Chromium) for a smexier looking experience.
